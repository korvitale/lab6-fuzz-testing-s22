import { calculateBonuses } from "./bonus-system.js";
const baseTest = function (mode) {
  return (amount, expected) => {
    it(`${mode} ${amount}`, (done) => {
      expect(calculateBonuses(mode, amount)).toBeCloseTo(expected, 3)
      done()
    })
  }
}

describe('Bonus system tests', () => {
  const expectedVals = {
    low: [0, 10, 500, 9999],
    mid: [10000, 25000, 49999],
    high: [50000, 75000, 99999],
    higher: [100000, 999999999999999]
  }
  const progMapping = {
    Standard: {
      low: 0.05,
      mid: 0.075,
      high: 0.1,
      higher: 0.125
    },
    Premium: {
      low: 0.1,
      mid: 0.15,
      high: 0.2,
      higher: 0.25
    },
    Diamond: {
      low: 0.2,
      mid: 0.3,
      high: 0.4,
      higher: 0.5
    },
    other: {
      low: 0,
      mid: 0,
      high: 0,
      higher: 0
    },
  }

  for(var prog in progMapping) {
    const expectedMappings = progMapping[prog];
    const testCase = baseTest(prog)

    for(var arrName in expectedMappings) {
      const expectedBonus = expectedMappings[arrName]

      expectedVals[arrName].forEach(amount => {
        testCase(amount, expectedBonus)
      });
    }
  }
});